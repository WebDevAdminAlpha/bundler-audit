module gitlab.com/gitlab-org/security-products/analyzers/bundler-audit/v2

require (
	github.com/sirupsen/logrus v1.7.0
	github.com/urfave/cli/v2 v2.3.0
	gitlab.com/gitlab-org/security-products/analyzers/command v1.1.0
	gitlab.com/gitlab-org/security-products/analyzers/common/v2 v2.23.0
	gitlab.com/gitlab-org/security-products/analyzers/report/v2 v2.1.0
)

go 1.13

# bundler-audit analyzer

Dependency Scanning for Ruby projects. It's based on [bundler-audit](https://github.com/rubysec/bundler-audit).

This analyzer is written in Go using
the [common library](https://gitlab.com/gitlab-org/security-products/analyzers/common)
shared by all analyzers.

The [common library](https://gitlab.com/gitlab-org/security-products/analyzers/common)
contains documentation on how to run, test and modify this analyzer.

## How to update the upstream Scanner

- Check for the latest version at https://github.com/rubysec/bundler-audit/tags.
- Compare with the value of `SCANNER_VERSION` in the [Dockerfile](./Dockerfile).
- If an update is available, create a branch where `SCANNER_VERSION` is updated and open a Merge Request.
- Check for possible new security vulnerabilities by following the process in [our handbook](https://about.gitlab.com/handbook/engineering/development/secure/composition-analysis/#security-checks-when-updating-an-upstream-scanner).
- Check for possible license update by following the process in [our handbook](https://about.gitlab.com/handbook/engineering/development/secure/composition-analysis/#license-check-when-updating-an-upstream-scanner).

## Running the analyzer in airgapped or internal networks

By default, this analyzer will make a network call to update its internal advisory DB (https://github.com/rubysec/ruby-advisory-db.git) at scan time.

1. To prevent any scan-time updates use `BUNDLER_AUDIT_UPDATE_DISABLED="true"`.
2. To ensure the advisory DB is up to date against a fork you control, use `BUNDLER_AUDIT_ADVISORY_DB_URL` and `BUNDLER_AUDIT_ADVISORY_DB_REF_NAME`.

Note:
* `BUNDLER_AUDIT_ADVISORY_DB_URL` is the Git URL of the advisory DB `bundler-audit` gem uses (forked from https://github.com/rubysec/ruby-advisory-db.git repository and following the same internal structure).
* `BUNDLER_AUDIT_ADVISORY_DB_REF_NAME` is Git the ref name in the advisory DB above (can be a commit hash, a branch, or a tag name).

## Contributing

Contributions are welcome, see [`CONTRIBUTING.md`](CONTRIBUTING.md) for more details.

## License

This code is distributed under the MIT license, see the [LICENSE](LICENSE) file.

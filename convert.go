package main

import (
	"bufio"
	"io"
	"path/filepath"
	"strings"

	"gitlab.com/gitlab-org/security-products/analyzers/bundler-audit/v2/metadata"
	"gitlab.com/gitlab-org/security-products/analyzers/report/v2"
)

const (
	filename = "Gemfile.lock"
)

func convert(reader io.Reader, prependPath string) (*report.Report, error) {
	vulns := parse(reader)
	issues := make([]report.Vulnerability, len(vulns))
	path := filepath.Join(prependPath, filename)
	for i, vuln := range vulns {
		issues[i] = vuln.Issue(path)
	}

	report := report.NewReport()
	report.Vulnerabilities = issues
	return &report, nil
}

func parse(reader io.Reader) []Vulnerability {

	vulns := []Vulnerability{} // found vulnerabilities
	var v *Vulnerability       // vulnerability currently parsed

	// to allocate a vulnerability if needed
	var allocate = func() *Vulnerability {
		if v == nil {
			v = &Vulnerability{}
		}
		return v
	}

	// to append current vulnerability if any
	var flush = func() {
		if v != nil {
			vulns = append(vulns, *v)
			v = nil // reset current vulnerability
		}
	}

	// scan document
	scanner := bufio.NewScanner(reader)
	for scanner.Scan() {
		line := scanner.Text()

		// append vuln if empty line
		if line == "" {
			flush()
			continue
		}

		// parse fields
		if fields := strings.SplitN(line, ":", 2); len(fields) > 1 {
			key := strings.TrimSpace(fields[0])
			value := strings.TrimSpace(fields[1])
			vuln := allocate()
			switch key {
			case "Name":
				vuln.Name = value
			case "Version":
				vuln.Version = value
			case "Advisory":
				vuln.Advisory = value
			case "Criticality":
				vuln.Criticality = value
			case "URL":
				vuln.URL = value
			case "Title":
				vuln.Title = value
			case "Solution":
				vuln.Solution = value
			}
		}
	}

	// flush in case there's no empty line after last vulnerability
	flush()

	return vulns
}

// Vulnerability describes a vulnerability found by bundler-audit.
type Vulnerability struct {
	Name        string
	Version     string
	Advisory    string
	Criticality string
	URL         string
	Title       string
	Solution    string
}

// Issue converts a vulnerability into a generic report.
func (v Vulnerability) Issue(filepath string) report.Vulnerability {
	return report.DependencyScanningVulnerability{report.Vulnerability{
		Message:  v.Title,
		Severity: report.ParseSeverityLevel(v.Criticality),
		Category: report.CategoryDependencyScanning,
		Scanner:  metadata.IssueScanner,
		Location: report.Location{
			File: filepath,
			Dependency: &report.Dependency{
				Package: report.Package{
					Name: v.Name,
				},
				Version: v.Version,
			},
		},
		Identifiers: v.identifiers(),
		Links:       report.NewLinks(v.URL),
		Solution:    v.Solution,
	}}.ToVulnerability()
}

func (v Vulnerability) identifiers() []report.Identifier {
	ids := []report.Identifier{}
	if id, ok := report.ParseIdentifierID(v.AdvisoryID()); ok {
		ids = append(ids, id)
	}
	return ids
}

// AdvisoryID sanitizes the advisory field and prepend "OSVDB-" when needed.
func (v Vulnerability) AdvisoryID() string {
	a := v.Advisory
	if len(a) > 0 && !strings.Contains(a, "-") {
		return "OSVDB-" + a
	}
	return a
}

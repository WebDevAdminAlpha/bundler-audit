FROM golang:1.15-alpine AS build

ENV CGO_ENABLED=0

WORKDIR /go/src/app
COPY . .
# build the analyzer binary and automatically set the AnalyzerVersion
# variable to the most recent version from the CHANGELOG.md file
RUN CHANGELOG_VERSION=$(grep -m 1 '^## v.*$' "CHANGELOG.md" | sed 's/## v//') && \
    PATH_TO_MODULE=`go list -m` && \
    go build -ldflags="-X '$PATH_TO_MODULE/metadata.AnalyzerVersion=$CHANGELOG_VERSION'" -o /analyzer

FROM ruby:2.7-slim

ARG BUNDLER_VERSION="2.1.4"
ENV BUNDLER_VERSION $BUNDLER_VERSION

ARG SCANNER_VERSION="0.7.0.1"
ENV SCANNER_VERSION $SCANNER_VERSION

ARG BUNDLER_AUDIT_ADVISORY_DB_URL="https://github.com/rubysec/ruby-advisory-db.git"
ARG BUNDLER_AUDIT_ADVISORY_DB_REF_NAME="master"
ENV BUNDLER_AUDIT_ADVISORY_DB_URL $BUNDLER_AUDIT_ADVISORY_DB_URL
ENV BUNDLER_AUDIT_ADVISORY_DB_REF_NAME $BUNDLER_AUDIT_ADVISORY_DB_REF_NAME
ARG BUNDLER_AUDIT_ADVISORY_DB_PATH="/bundler-audit/.local/share/ruby-advisory-db"
ENV BUNDLER_AUDIT_ADVISORY_DB_USER_HOME="/bundler-audit"

RUN set -ex; \
    \
    apt-get update; \
    apt-get install -y --no-install-recommends git; \
    apt-get upgrade -y; \
    rm -rf /var/lib/apt/lists/*; \
    \
    mkdir -p $BUNDLER_AUDIT_ADVISORY_DB_PATH; \
    git clone --branch $BUNDLER_AUDIT_ADVISORY_DB_REF_NAME $BUNDLER_AUDIT_ADVISORY_DB_URL $BUNDLER_AUDIT_ADVISORY_DB_PATH; \
    \
    gem install bundler:$BUNDLER_VERSION bundler-audit:$SCANNER_VERSION; \
    bundle audit update; \
    \
    # give write access to CA certificates (OpenShift)
    mkdir -p /etc/ssl/certs/; \
    touch /etc/ssl/certs/ca-cert-additional-gitlab-bundle.pem; \
    chmod g+w /etc/ssl/certs/ca-cert-additional-gitlab-bundle.pem; \
    \
    # give write access to /etc/gitconfig where cacert writes sslCAInfo (OpenShift)
    touch /etc/gitconfig; \
    chmod g+w /etc/gitconfig; \
    \
    # give write access to vulnerability database (OpenShift)
    chmod -R g+w $BUNDLER_AUDIT_ADVISORY_DB_PATH; \
    echo "done"

# set user HOME to a directory where any user can write (OpenShift)
ENV HOME "/tmp"

COPY --from=build /analyzer /analyzer
ENTRYPOINT []
CMD ["/analyzer", "run"]
